# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-bundler
_gemname=bundler
pkgver=2.3.5
pkgrel=0
pkgdesc="Manage an application's gem dependencies"
url="https://bundler.io/"
arch="noarch"
license="MIT"
depends="ruby"
makedepends="ruby-rake"
subpackages="$pkgname-doc"
source="https://github.com/rubygems/rubygems/archive/bundler-v$pkgver.tar.gz
	manpages.patch
	"
builddir="$srcdir/rubygems-bundler-v$pkgver/bundler"
options="!check"  # tests require deps not available in main repo

build() {
	rake build_metadata
	gem build $_gemname.gemspec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	local n; for n in 1 5; do
		mkdir -p "$pkgdir"/usr/share/man/man$n
		mv "$gemdir"/gems/$_gemname-$pkgver/lib/bundler/man/*.$n "$pkgdir"/usr/share/man/man$n/
	done

	rm -rf "$gemdir"/cache \
		"$gemdir"/build_info \
		"$gemdir"/doc \
		"$gemdir"/gems/$_gemname-$pkgver/man \
		"$gemdir"/gems/$_gemname-$pkgver/*.md
}

sha512sums="
82fac8fdc9325727fa20220b5e62a0ef81027cbdd7b0e527faaa85716df7ed17f038dcc78ec79a655fb9d8a28fe38ab2b9e2b62cbfef752b9a27d79decf9fbb6  bundler-v2.3.5.tar.gz
5c9cc8046120360f9daa3d94da092c8db452672b8fb46b1a8188fae690e4362a090c0fcce35487399aedfec2f7f6b212c0d873a065475871b76d09313964596a  manpages.patch
"
