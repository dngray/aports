# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Maxim Karasev <begs@disroot.org>
pkgname=hut
pkgver=0.0.0_git20220114
_commit=67389900272027b60424947c0e6ab05370e8aae1
pkgrel=0
pkgdesc="command-line tool for sr.ht"
url="https://sr.ht/~emersion/hut"
arch="all"
license="AGPL-3.0-only"
makedepends="go scdoc"
subpackages="$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~emersion/hut/archive/$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"
options="!check" # no test suite

build() {
	make all

	./hut completion zsh >$pkgname.zsh
	./hut completion bash >$pkgname.bash
	./hut completion fish >$pkgname.fish
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install

	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname.bash

	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

sha512sums="
aa55664063c14b1e7e3704b80c9d95cb09b6e9757af324ad57d613e4afb7905045854f582634b819d7fbec1b90da0c427132ddd25f7e798880a324e73129e345  hut-0.0.0_git20220114.tar.gz
"
