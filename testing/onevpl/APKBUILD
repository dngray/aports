# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=onevpl
pkgver=2022.0.3
pkgrel=0
pkgdesc="oneAPI Video Processing Library"
url="https://github.com/oneapi-src/oneVPL"
arch="x86_64" # only x86_64 supported
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/oneVPL/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/oneVPL-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_PREVIEW=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TOOLS=OFF \
		-DINSTALL_EXAMPLE_CODE=OFF \
		-DBUILD_TESTS=ON
	cmake --build build
}

check() {
	ctest -j $JOBS --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	amove usr/share
}

sha512sums="
cc4cdad31db67666c9a95c813eb69123b7988551c9d7812cf84104e2b5390d4226e420cce2605aa9d9e2b595b219727ab037795c152f2ebf84d935e7c1b64d22  onevpl-2022.0.3.tar.gz
"
