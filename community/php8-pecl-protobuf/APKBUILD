# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php8-pecl-protobuf
_extname=protobuf
pkgver=3.19.3
pkgrel=0
pkgdesc="PHP 8.0 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data - PECL"
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
depends="php8-common"
makedepends="php8-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize8
	./configure --prefix=/usr --with-php-config=php-config8
	make
}

check() {
	# Test suite is not a part of pecl release.
	php8 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname.so" > $_confdir/$_extname.ini
}

sha512sums="
af98ebcd80b8bdff187263517674022f6430808d57e72a7cf2880315254552e82f51b085bc9c4dd7bfc76943d9c1fa04816951fa0711d7cbce6e322727ca8fd1  php-pecl-protobuf-3.19.3.tgz
"
