# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=cosign
pkgver=1.5.0
pkgrel=0
pkgdesc="container signing tool with support for ephemeral keys and Sigstore signing"
url="https://github.com/sigstore/cosign"
arch="all"
license="Apache-2.0"
# pcsc-lite-libs needed at runtime for smartcard support
depends="pcsc-lite-libs"
makedepends="go pcsc-lite-dev"
# NOTE: We can't use the default bashcomp, etc splitters because they take everything,
# the default splitters should be improved somehow.
subpackages="
	sget
	sget-bash-completion:_sget_bashcomp
	sget-fish-completion:_sget_fishcomp
	sget-zsh-completion:_sget_zshcomp

	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/sigstore/cosign/archive/v$pkgver/cosign-$pkgver.tar.gz"

build() {
	mkdir build
	go build -o build/ -tags -tags=pivkey,pkcs11key "$builddir"/...

	for i in bash fish zsh; do
		"$builddir"/build/cosign completion $i > "$builddir"/cosign.$i
		"$builddir"/build/sget completion $i > "$builddir"/sget.$i
	done
}

check() {
	make test
}

package() {
	install -Dm755 "$builddir"/build/cosign "$pkgdir"/usr/bin/cosign
	install -Dm755 "$builddir"/build/sget "$pkgdir"/usr/bin/sget

	for i in cosign sget; do
		install -Dm644 "$builddir"/$i.bash "$pkgdir"/usr/share/bash-completion/completions/$i
		install -Dm644 "$builddir"/$i.fish "$pkgdir"/usr/share/fish/completions/$i.fish
		install -Dm644 "$builddir"/$i.zsh "$pkgdir"/usr/share/zsh/site-functions/_$i
	done
}

sget() {
	pkgdesc="secure container image fetching tool"
	amove /usr/bin/sget
}

_sget_bashcomp() {
	amove /usr/share/bash-completion/completions/sget
}

_sget_fishcomp() {
	amove /usr/share/fish/completions/sget.fish
}

_sget_zshcomp() {
	amove /usr/share/zsh/site-functions/_sget
}

sha512sums="
fc7768e37bc7236d2275a51e4b3f0854051d018af62e25d7c20ac3d79f7deacde181b650752da9a859e24585915bbcaf2f1d34f73747f0ffe9871109e1a3e019  cosign-1.5.0.tar.gz
"
